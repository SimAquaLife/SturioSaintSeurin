
obsFile = newFile();
obsFile << "<hashtable></hashtable>";

arguments = ['-simDuration','36','-simBegin','2015','-timeStepDuration','1',
  '-groups', groupFile.getAbsolutePath(),'-env',envFile.getAbsolutePath(),'-observers',obsFile.getAbsolutePath(), 
  '-RNGStatusIndex', replicat
  ] as String[]

path = "aquaticWorld.aquaNismsGroupsList.0.processes.processesEachStep."
  
Pilot.init()

BatchRunner.parseArgs(arguments,false,true,false);

BatchRunner.load();
//ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"0.nbWithNaturalDiet", nbIn);

Pilot.run()
femaleInLastReproduction = 
ReflectUtils.getValueFromPath(Pilot.getInstance(),"aquaticWorld.aquaNismsGroupsList.0.getNumberOfFemaleInMaturation");
femaleInReproductions = 
ReflectUtils.getValueFromPath(Pilot.getInstance(),"aquaticWorld.aquaNismsGroupsList.0.getFemaleSpawnersString");
larvaeProductions = 
ReflectUtils.getValueFromPath(Pilot.getInstance(),"aquaticWorld.aquaNismsGroupsList.0.getLarvaeProductionString");