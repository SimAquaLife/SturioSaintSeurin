// pour une execution sur ma machine
// cd C:\Program Files\OpenMOLE
// openmole.bat -c -p D:\workspace\SturioSaintSeurin\target\${symbolic.name}_0.0.1-SNAPSHOT.jar -s D:\workspace\SturioSaintSeurin\openMole\ddpSaintSeurin.scala 2>D:\workspace\SturioSaintSeurin\openMole\erreur.txt

//1>&2 

// pour un lancement sur le serveur de calcul
// /usr/local/openmole/openmole -c -configuration /home/win/BORDEAUX/patrick.lambert/SturioSaintSeurin/ -p /home/win/BORDEAUX/patrick.lambert/SturioSaintSeurin/target/SturioSaintSeurin-0.0.1-SNAPSHOT.jar -s /home/win/BORDEAUX/patrick.lambert/SturioSaintSeurin/openMole/SD4ref.scala 2>/home/win/BORDEAUX/patrick.lambert/SturioSaintSeurin/openMole/erreur.txt

import org.openmole.plugin.task.groovy._
import org.openmole.plugin.domain.range._
import org.openmole.plugin.hook.display._
import org.openmole.core.model.execution.Environment
import org.openmole.plugin.domain.collection._
import org.openmole.plugin.sampling.combine._
import org.openmole.plugin.domain.distribution._
import org.openmole.plugin.hook.display._
import org.openmole.plugin.hook.file._
import org.openmole.plugin.environment.pbs._

// pour une execution sur ma machine
val wd = "D:/workspace/SturioSaintSeurin/"

// pour une execution sur le serveur
//val wd ="/home/win/BORDEAUX/patrick.lambert/SturioSaintSeurin/"

// lancement sur la ferme de calcul de Clermont
//logger.level("FINE")
//val env = PBSEnvironment("rougier", "calcul64.clermont.cemagref.fr")

// ================================= prototype
// ----------- input 
val replicat = Prototype[Int]("replicat")

val nbIn = Prototype[Int]("nbIn")

val groupFile = Prototype[File]("groupFile")
val envFile = Prototype[File]("envFile")

// ---------------- output
val femaleInLastReproduction = Prototype[Int]("femaleInLastReproduction")
val femaleInReproductions = Prototype[String]("femaleInReproductions")
val larvaeProductions = Prototype[String]("larvaeProductions")

// ======================================== exploration
val explo = ExplorationTask("explo",
	Factor(nbIn, 25 to 50 by 5 toDomain) x
 	Factor(replicat, 1 to 10000 by 1 toDomain)
   )
	
// tache Groovy
val model = GroovyTask("model", scala.io.Source.fromFile(wd + "openMole/ddpSaintSeurin.groovy").mkString)
model addImport "fr.cemagref.simaqualife.extensions.pilot.BatchRunner"
model addImport "fr.cemagref.simaqualife.pilot.Pilot"
model addImport "miscellaneous.ReflectUtils"
model addInput replicat
model addInput nbIn
model addParameter (groupFile -> new File(wd+"data/input/juvenileStock.xml"))
model addParameter (envFile -> new File(wd+"data/input/SaintSeurin.xml"))

model addOutput replicat
model addOutput nbIn
model addOutput femaleInLastReproduction
model addOutput femaleInReproductions
model addOutput	larvaeProductions

	// sur l'ordi
	val h = AppendToCSVFileHook(wd + "data/output/ddpSaintSeurin.txt")

	//sur le serveur de calcul (pas la m�me version d'openMole que sur ma machine)
	//val h = new AppendToCSVFileHook(wd + "data/output/ddpSaintSeurin.txt")


val ex = explo -< (model hook h) toExecution
ex.start
ex.waitUntilEnded