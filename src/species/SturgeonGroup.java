/**
 * Patrick.Lambert
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package species;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;



import species.Sturgeon.Gender;
import species.Sturgeon.Status;
import environment.Station;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 *
 */
public class SturgeonGroup extends AquaNismsGroup<Sturgeon, Station> {

	private transient Map<String, List<Sturgeon>> batches;
	private transient List<Integer> femaleSpawnerNbHistory;
	private transient List<Double> larvaeProductionHistory;
	private transient double annualLarvaeProduction;

	@Override
	public void initTransientParameters(Pilot pilot) throws IllegalArgumentException,
	IllegalAccessException, InvocationTargetException {
		// TODO Auto-generated method stub
		super.initTransientParameters(pilot);
		batches = new TreeMap<String, List<Sturgeon>>();

		femaleSpawnerNbHistory = new LinkedList<Integer>();
		larvaeProductionHistory = new LinkedList<Double>();

	}

	@Override
	public void addAquaNism(Sturgeon fish) {
		super.addAquaNism(fish);

		if(batches.containsKey(fish.getBatchName())){
			batches.get(fish.getBatchName()).add(fish);
		}
		else {
			List<Sturgeon> batch = new ArrayList<Sturgeon>();
			batch.add(fish);
			batches.put(fish.getBatchName(), batch);
		}

	}

	@Override
	public void removeAquaNism(Sturgeon fish) {
		// TODO Auto-generated method stub
		super.removeAquaNism(fish);

		batches.get(fish.getBatchName()).remove(fish);
	}


	public void printNumberInBatches(){
		int total=0;
		for (Entry<String, List<Sturgeon>> entry : batches.entrySet()){
			System.out.println(" "+ entry.getKey()+ ":\t"+entry.getValue().size());
			total += entry.getValue().size();
		}
		System.out.println("-->"+total+"vs"+this.getAquaNismsList().size());
	}

	@Observable(description = "Fish number")
	public int getFishNumber() {
		if ( this.getAquaNismsList() != null)
			return this.getAquaNismsList().size();
		else
			return 0;
	}

	public Integer[] getFemaleHistory() {
		return (Integer[]) femaleSpawnerNbHistory.toArray();
	}

	@Observable(description = "nb of female ready to spawn")
	public int getNumberOfFemaleInMaturation() {
		int nb=0;
		for (Sturgeon fish : this.getAquaNismsList()){
			if (fish.getGender() == Gender.FEMALE & fish.isReadyToSpawn()){
				nb++;
			}
		}
		return nb;
	}

	@Observable(description = "nb of male ready to spawn")
	public int getNumberOfMaleInMaturation() {
		int nb=0;
		for (Sturgeon fish : this.getAquaNismsList()){
			if (fish.getGender() == Gender.MALE & fish.isReadyToSpawn()){
				nb++;
			}
		}
		return nb;
	}

	@Observable(description = "nb of normal fish")
	public int getNormalFishNumber() {
		int nb=0;
		for (Sturgeon fish : this.getAquaNismsList()){
			if (fish.getStatus() == Status.NORMAL){
				nb++;
			}
		}
		return nb;
	}

	@Observable(description = "nb of deformed fish")
	public int getDeformedFishNumber() {
		int nb=0;
		for (Sturgeon fish : this.getAquaNismsList()){
			if (fish.getStatus() == Status.DEFORMED){
				nb++;
			}
		}
		return nb;
	}

	@Observable(description = "annual larvae production")
	public double getAnnualLarvaeProduction() {
		return Math.round(annualLarvaeProduction * 1000.)/1000.;
	}

	public void setAnnualLarvaeProduction(double annualLarvaeProduction) {
		this.annualLarvaeProduction = annualLarvaeProduction;
	}

	public void setLarvaeProductionHistory(List<Double> larvaeProductionHistory) {
		this.larvaeProductionHistory = larvaeProductionHistory;
	}

	public Map<String, List<Sturgeon>> getBatches() {
		return batches;
	}

	public List<Integer> getFemaleSpawnerNbHistory() {
		return femaleSpawnerNbHistory;
	}

	public List<Double> getLarvaeProductionHistory() {
		return larvaeProductionHistory;
	}

	@Observable(description = "female spawner history")
	public Integer[] getFemaleSpawnersArray() {
		Integer[] output= new Integer[femaleSpawnerNbHistory.size()];
		for(int i=0; i < femaleSpawnerNbHistory.size(); i++){
			output[i] =femaleSpawnerNbHistory.get(i);
		}
		return  output;
	}

	@Observable(description = "female spawner history")
	public String getFemaleSpawnersString() {
		String output="";
		String sep="";
		for (int i=0; i< femaleSpawnerNbHistory.size(); i++){
			output=output.concat(sep).concat(femaleSpawnerNbHistory.get(i).toString());
			sep=", ";
		}
		return  output;
	}

	@Observable(description = "larvae hsitory history")
	public String getLarvaeProductionString() {
		String output="";
		String sep="";
		for (int i=0; i< larvaeProductionHistory.size(); i++){
			output=output.concat(sep).concat(larvaeProductionHistory.get(i).toString());
			sep=", ";
		}
		return  output;
	}
}
