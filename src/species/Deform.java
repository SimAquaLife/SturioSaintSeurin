/**
 * Patrick.Lambert
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package species;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import species.Sturgeon.Diet;
import species.Sturgeon.Status;
import umontreal.iro.lecuyer.probdist.GammaDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 *
 */
public class Deform extends AquaNismsGroupProcess<Sturgeon, SturgeonGroup> {

	private double scaleDeform = 0.3203588; // as in R: (inverse in in Lecuyer)
	private double shapeDeform = 6.55763282;
	private double piDeformForNaturalDiet = 0.05565945;
	private double piDeformForMixedDiet = 0.5463054;

	private transient UniformGen genUniform;
	private transient GammaDist gammaDist;
	
	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Deform())); }
	
	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		genUniform = new UniformGen( pilot.getRandomStream(),0.,1.);
		gammaDist = new GammaDist(shapeDeform, 1/scaleDeform);
	}
	
	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 */
	@Override
	public void doProcess(SturgeonGroup stock) {
		for (Sturgeon fish : stock.getAquaNismsList()){
			if (fish.getStatus() == Status.NORMAL){
				//System.out.println("  "+fish.getAge()+" : " + getProbaToDeform(fish));
				if (genUniform.nextDouble() < getProbaToDeform(stock.getPilot(), fish)){
					fish.setStatus(Status.DEFORMED);
				}
			}
		}
	}
	
	public double getProbaToDeform(Pilot pilot, Sturgeon fish){
		double proba=0.;
		double pi = (fish.getDiet() == Diet.NATURAL) ? piDeformForNaturalDiet : piDeformForMixedDiet;
		double probaT = gammaDist.cdf(fish.getAge());
		double probaTplus = gammaDist.cdf(fish.getAge() + pilot.getTimeStepDuration()); 
		proba = pi * (probaTplus - probaT) / (1 - pi * probaT);
		return proba;
	}

}
