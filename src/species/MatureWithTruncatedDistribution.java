/**
 * 
 */
package species;

import miscellaneous.TruncatedGammaDist;

import org.openide.util.lookup.ServiceProvider;

import species.Sturgeon.Gender;
import species.Sturgeon.Stage;
import umontreal.iro.lecuyer.probdist.PoissonDist;
import umontreal.iro.lecuyer.randvar.UniformGen;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MatureWithTruncatedDistribution extends
AquaNismsGroupProcess<Sturgeon, SturgeonGroup> {


	private double probTruncature = 0.01;

	private double femaleMeanAgeAtMaturation = 16.4; // in year
	private double femaleSdAgeAtMaturation = 2.7;
	private double femaleMeanInterSpawningInterval = 4.;

	private double maleMeanAgeAtMaturation = 8; // in year
	private double maleSdAgeAtMaturation = 1.4;
	private double maleMeanInterSpawningInterval = 1.5;

	private transient double lag;
	private transient TruncatedGammaDist femaleMaturationDist;
	private transient TruncatedGammaDist maleMaturationDist;
	private transient PoissonDist femaleInterSpawningIntervalDist;
	private transient PoissonDist maleInterSpawningIntervalDist;
	private transient UniformGen genUniform;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MatureWithTruncatedDistribution())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		genUniform = new UniformGen(pilot.getRandomStream(),0.,1.);
		
		// for female first maturation
		double alpha, lambda;
		alpha = Math.pow( femaleMeanAgeAtMaturation	/ femaleSdAgeAtMaturation, 2);
		lambda = femaleMeanAgeAtMaturation /Math.pow(femaleSdAgeAtMaturation, 2);
		femaleMaturationDist = new TruncatedGammaDist(alpha, lambda, probTruncature);

		// for male first maturation
		alpha = Math.pow(maleMeanAgeAtMaturation / maleSdAgeAtMaturation, 2);
		lambda = maleMeanAgeAtMaturation / Math.pow(maleSdAgeAtMaturation, 2);
		maleMaturationDist = new TruncatedGammaDist(alpha, lambda, probTruncature);

		// interval between maturation 
		lag =1.; // to avoid forgetting the proba for interval =0
		femaleInterSpawningIntervalDist = new PoissonDist(femaleMeanInterSpawningInterval-lag);
		maleInterSpawningIntervalDist = new PoissonDist(maleMeanInterSpawningInterval-lag);

	}

	@Override
	public void doProcess(SturgeonGroup group) {

		double age, laggedTimeFromLastReproduction, probaToMature;

		for (Sturgeon fish : group.getAquaNismsList()) {
			age = fish.getAge();

			// new maturation
			if (fish.getStage() == Stage.MATURE){
				laggedTimeFromLastReproduction = age - fish.getAgeAtLastReproduction() - lag;
				if (fish.getGender() == Gender.FEMALE){
					probaToMature = (femaleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction) - 
							femaleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction -1.))
							/ (1 - femaleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction - 1.));
				}
				else {
					probaToMature = (maleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction) - 
							maleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction -1.))
							/ (1 - maleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction - 1.));
				}

				// 
				if (genUniform.nextDouble() < probaToMature){
					fish.setReadyToSpawn(true);
					fish.setAgeAtLastReproduction();
				}
				else {
					fish.setReadyToSpawn(false);
				}
			}
			// first maturation
			else {
				if (fish.getGender() == Gender.FEMALE){
					// compute the prob to mature between age and age-1 
					// knowing that it was not mature before
					probaToMature = (femaleMaturationDist.cdf(age) - femaleMaturationDist.cdf(age -1.))
							/ (1 - femaleMaturationDist.cdf(age - 1.));
				}
				else {
					probaToMature = (maleMaturationDist.cdf(age) - maleMaturationDist.cdf(age -1.))
							/ (1 - maleMaturationDist.cdf(age - 1.));
				}
				//System.out.println(fish.getGender() +": "+fish.getAge() + "-->"+ probaToMature);

				if (genUniform.nextDouble() < probaToMature){
					fish.setStage(Stage.MATURE);
					fish.setReadyToSpawn(true);
					fish.setAgeAtLastReproduction();
				}
			}
		}
		
/*		int nb=0;
		for (Sturgeon fish : group.getAquaNismsList()){
			if (fish.getGender() == Gender.FEMALE & fish.isReadyToSpawn()){
				nb++;
			}
		}
		group.getFemaleSpawnerNbHistory().add(nb);*/
	}
	//System.out.println(this.getClass()+ " is finished");
}

