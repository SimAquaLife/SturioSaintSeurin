/**
 * Patrick.Lambert
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package species;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

import species.Sturgeon.Diet;
import species.Sturgeon.Gender;
import species.Sturgeon.Stage;
import species.Sturgeon.Status;
import umontreal.iro.lecuyer.randvar.UniformGen;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 *
 */
public class Enter extends AquaNismsGroupProcess<Sturgeon, SturgeonGroup> {

	private int nbWithNaturalDiet=25;
	private int nbWithMixedDiet=25;

	private transient UniformGen genUniform;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Enter())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		genUniform = new UniformGen(pilot.getRandomStream(),0.,1.);
	}

	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 */
	@Override
	public void doProcess(SturgeonGroup group) {

		int cohort = (int) group.getPilot().getCurrentTime();
		int age = 0;

		// fish with natural diet ( not deformed)
		for (int i=0; i < nbWithNaturalDiet; i++){
			Sturgeon sturgeon = new Sturgeon(group.getPilot(), group.getEnvironment().getTanks(), cohort, age, Stage.IMMATURE, 
					(genUniform.nextDouble() <0.5) ? Gender.FEMALE : Gender.MALE, 
							 Diet.NATURAL, Status.NORMAL);
			group.addAquaNism(sturgeon);
		}
		//  fish with mixed diet (not Deformed)
		for (int i=0; i < nbWithMixedDiet; i++){
			Sturgeon sturgeon = new Sturgeon(group.getPilot(), group.getEnvironment().getTanks(), cohort, age, Stage.IMMATURE, 
					(genUniform.nextDouble() <0.5) ? Gender.FEMALE : Gender.MALE, 
					Diet.MIXED,	Status.NORMAL);
			group.addAquaNism(sturgeon);
		}

		//System.out.println(group.getFishNumber());
		//group.printNumberInBatches();
	}
}
