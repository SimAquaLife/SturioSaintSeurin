/**
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package species;

import environment.Tanks;
import environment.Station;
import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 *
 */
public class Sturgeon extends AquaNism<Tanks, Station>{
	
	public static enum Stage {IMMATURE, MATURE};
	public static enum Gender {FEMALE, MALE}; 
	public static enum Diet {NATURAL, MIXED};
	public static enum Status {NORMAL, DEFORMED};
	
	private final int cohort;
	private final Gender gender;
	private final Diet diet;
	private Stage stage;
	private Status status;
	private int age;
	private String name;

	private int numberOfReproduction;
	private double ageAtLastReproduction;
	private boolean readyToSpawn;

	/**
	 * 
	 * @param position
	 * @param stage
	 * @param gender
	 * @param diet
	 * @param isDeformed
	 */
	
	public Sturgeon(Pilot pilot, Tanks position, int cohort, int age, Stage stage, Gender gender, Diet diet,
			Status status, String name, double ageAtLastReproduction) {
		super(pilot, position);
		this.cohort = cohort;
		this.age = age;
		this.stage = stage;
		this.gender = gender;
		this.diet = diet;
		this.status = status;
		this.ageAtLastReproduction = ageAtLastReproduction;
		this.name = name;
		
		this.numberOfReproduction = 0;
		this.readyToSpawn = false;
	}
	
	public Sturgeon(Pilot pilot, Tanks position, int cohort, int age, Stage stage, Gender gender, Diet diet,
			Status status) {
		this(pilot, position, cohort, age, stage, gender, diet, status, "", Double.NaN );
	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Gender getGender() {
		return gender;
	}

	public Diet getDiet() {
		return diet;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public int getAge() {
		return age;
	}
	
	public void incrementAge(){
		this.age++;
	}

	public int getCohort() {
		return cohort;
	}
	
	public String getBatchName(){
		return String.valueOf(this.getCohort()).concat("-").concat(this.getDiet().toString());
	}
	
	public int getNumberOfReproduction() {
		return numberOfReproduction;
	}

	public void incrementNumberOfReproduction() {
		this.numberOfReproduction += 1;
	}
	
	public double getAgeAtLastReproduction() {
		return ageAtLastReproduction;
	}

	public void setAgeAtLastReproduction() {
		this.ageAtLastReproduction = this.age;
	}
	
	public boolean isReadyToSpawn() {
		return readyToSpawn;
	}

	public void setReadyToSpawn(boolean readyToSpawn) {
		this.readyToSpawn = readyToSpawn;
	}
	
}
