/**
 * Patrick.Lambert
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package species;

import java.io.FileReader;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

import species.Sturgeon.Diet;
import species.Sturgeon.Gender;
import species.Sturgeon.Stage;
import species.Sturgeon.Status;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *
 */
public class PopulateWithActualCaptiveStock extends AquaNismsGroupProcess<Sturgeon, SturgeonGroup> {

	private String sturgeonStockName ="data/input/maturation geniteurs juin2014.csv";
	private Boolean fishWithPb = true;
	private int presentYear = 2014;
	
	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new PopulateWithActualCaptiveStock())); }


	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 */
	@Override
	public void doProcess(SturgeonGroup group) {

		int cohort, birthYear, YearlastMaturation,pb;
		String alim, stage, sex, name, status;

		try {
			// open the file
			FileReader reader = new FileReader(sturgeonStockName);

			// Parsing the file
			Scanner scanner = new Scanner(reader);
			scanner.useLocale(Locale.ENGLISH); // to have a comma as decimal separator !!!
			scanner.useDelimiter(Pattern.compile("(;|\r\n|\n)"));

			// skip the first line
			scanner.nextLine();
			//System.out.println(scanner.nextLine());
			while (scanner.hasNext()) {
				cohort = scanner.nextInt();
				birthYear = scanner.nextInt();
				name= scanner.next();
				//System.out.print(name+ " ");
				scanner.next(); // skip colum
				scanner.next(); // skip colum
				sex = scanner.next();
				alim= scanner.next();
				status = scanner.next();
				stage = scanner.next();
				YearlastMaturation = scanner.nextInt();
				pb = scanner.nextInt();
				scanner.next(); // skip colum
				scanner.next(); // skip colum
				scanner.next(); // skip colum

				//public Sturgeon(Tanks position, int cohort, int age, Stage stage, Gender gender, Diet diet,
				//		Status status, String name, double ageAtLastReproduction);
				
				if (fishWithPb | (!fishWithPb & pb == 0 )){
					Sturgeon sturgeon = new Sturgeon(group.getPilot(), group.getEnvironment().getTanks(), cohort, (presentYear-birthYear), 
							(stage.equalsIgnoreCase("mature") ? Stage.MATURE : Stage.IMMATURE), 
							(sex.equalsIgnoreCase("femelle") ? Gender.FEMALE : Gender.MALE), 
							(alim.equalsIgnoreCase("alim naturelle") ? Diet.NATURAL : Diet.MIXED),
							(status.equalsIgnoreCase("tordu") ? Status.DEFORMED : Status.NORMAL),
							name, 
							(YearlastMaturation==-1 ? Double.NaN :(presentYear-birthYear-YearlastMaturation)));
					group.addAquaNism(sturgeon);
				}
			}
			//System.out.println();
			//System.out.println(group.getFishNumber()+" fish in the captive stock");
			scanner.close();
			reader.close();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//System.out.println(group.getFishNumber());
		//group.printNumberInBatches();

	}


}
