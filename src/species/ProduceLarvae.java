/**
 * Patrick.Lambert
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package species;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import species.Sturgeon.Gender;
import umontreal.iro.lecuyer.probdist.LognormalDist;
import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.randvar.LognormalGen;
import umontreal.iro.lecuyer.randvar.NormalGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 *
 */
public class ProduceLarvae extends AquaNismsGroupProcess<Sturgeon, SturgeonGroup> {

	private double pi=.75;
	private double mu=4.3470;
	private double sigma=0.7806;
	private double maxProd=250.;
	transient NormalGen genAleaNormal;
	transient LognormalGen genAleaLognormal;
	
	@Override
	public void initTransientParameters(Pilot pilot) {
		super.initTransientParameters(pilot);
		
		genAleaNormal = new NormalGen(pilot.getRandomStream(), new NormalDist(0.,1.));
		genAleaLognormal = new LognormalGen(pilot.getRandomStream(), new LognormalDist(mu, sigma));
	}

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new ProduceLarvae())); }

	
	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 */
	@Override
	public void doProcess(SturgeonGroup group) {
		int nbFemale=0;
		double nbLarvae=0.; // in thousound of fish
		for (Sturgeon fish : group.getAquaNismsList()){
			if (fish.getGender() == Gender.FEMALE & fish.isReadyToSpawn()){
				nbFemale++;
				
				if (genAleaNormal.nextDouble()<pi){
					double nlarvae=Math.min(maxProd,genAleaLognormal.nextDouble());
					nbLarvae +=nlarvae;
					//System.out.println(nlarvae);
				}
			}
		}
		group.getFemaleSpawnerNbHistory().add(nbFemale);
		group.getLarvaeProductionHistory().add(nbLarvae);
		group.setAnnualLarvaeProduction(nbLarvae);
	}

}
