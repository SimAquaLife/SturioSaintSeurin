/**
 * Patrick.Lambert
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package species;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;


import species.Sturgeon.Status;
import umontreal.iro.lecuyer.probdist.WeibullDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 *
 */
public class Die extends AquaNismsGroupProcess<Sturgeon, SturgeonGroup> {

	private double dieScaleForNormal = 0.01877447;
	private double dieScaleForDeformed = 0.40354444;
	private double dieShape = 1.;

	private transient UniformGen genUniform;
	private transient WeibullDist weibullDistForNormal;
	private transient WeibullDist weibullDistForDeformed;
	
	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Die())); }
	
	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		genUniform = new UniformGen( pilot.getRandomStream(),0.,1.);
		weibullDistForNormal = new WeibullDist(dieShape, dieScaleForNormal,  0.);
		weibullDistForDeformed = new WeibullDist(dieShape, dieScaleForDeformed, 0.);
	}
	
	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 */
	@Override
	public void doProcess(SturgeonGroup stock) {
		List<Sturgeon> deadFish = new ArrayList<Sturgeon>();
		for (Sturgeon fish : stock.getAquaNismsList()){
			//System.out.println(fish.getAge()+":"+getProbaToDie(fish));
			if (genUniform.nextDouble() < getProbaToDie(stock.getPilot(), fish)){
				deadFish.add(fish);
			}
		}
		// remove fish from the list
		for (Sturgeon fish : deadFish){
			stock.removeAquaNism(fish);
		}
		
		//stock.printNumberInBatches();
	}
	
	public double getProbaToDie(Pilot pilot, Sturgeon fish){
		double probaT, probaTplus;
		if (fish.getStatus()  == Status.DEFORMED){
			probaT = weibullDistForDeformed.cdf(fish.getAge());
			probaTplus = weibullDistForDeformed.cdf(fish.getAge() + pilot.getTimeStepDuration()); 
		} 
		else {
			probaT = weibullDistForNormal.cdf(fish.getAge());
			probaTplus = weibullDistForNormal.cdf(fish.getAge() + pilot.getTimeStepDuration()); 
		}
			
		return ((probaTplus - probaT) / (1 - probaT));
	
	}

}
