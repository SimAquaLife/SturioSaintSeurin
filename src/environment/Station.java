/**
 * Patrick.Lambert
 * @author Patrick Lambert
 * @copyright Copyright (c) 2014, Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
package environment;

import java.util.List;

import species.Sturgeon;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.spatial.Environment;

/**
 *
 */
public class Station extends Environment<Tanks, Sturgeon> {
	
	Tanks tanks;

	public Station() {
		super();
		tanks = new Tanks();
	}

	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.spatial.Environment#addAquaNism(fr.cemagref.simaqualife.kernel.AquaNism, fr.cemagref.simaqualife.kernel.AquaNismsGroup)
	 */
	@Override
	public void addAquaNism(Sturgeon aquaNism, AquaNismsGroup group) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.spatial.Environment#removeAquaNism(fr.cemagref.simaqualife.kernel.AquaNism, fr.cemagref.simaqualife.kernel.AquaNismsGroup)
	 */
	@Override
	public void removeAquaNism(Sturgeon aquaNism, AquaNismsGroup group) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.spatial.Environment#moveAquaNism(fr.cemagref.simaqualife.kernel.AquaNism, fr.cemagref.simaqualife.kernel.AquaNismsGroup, fr.cemagref.simaqualife.kernel.spatial.Position)
	 */
	@Override
	public void moveAquaNism(Sturgeon aquaNism, AquaNismsGroup group,
			Tanks destination) {
		// nothing to do
		
	}

	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.spatial.Environment#getNeighbours(fr.cemagref.simaqualife.kernel.spatial.Position)
	 */
	@Override
	public List<Tanks> getNeighbours(Tanks position) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Tanks getTanks(){
		return tanks;
	}

}
