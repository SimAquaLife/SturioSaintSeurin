package miscellaneous;

import fr.cemagref.simaqualife.pilot.Pilot;
import umontreal.iro.lecuyer.randvar.NormalGen;
import umontreal.iro.lecuyer.randvar.UniformGen;

public class Miscellaneous {

	static public long binomialForSuperIndividual(Pilot pilot, long amount, double succesProba, long threshold) {
		long amountWithSuccess;
		if (amount > threshold) { // use a normal approximation for huge amount 
			amountWithSuccess= -1;
			while (amountWithSuccess <0 | amountWithSuccess>amount){
				amountWithSuccess = Math.round(NormalGen.nextDouble(pilot.getRandomStream(),0.,1.) * Math.sqrt(succesProba * (1 - succesProba) * amount) 
						+ succesProba * amount);
			}
		}
		else {
			amountWithSuccess= 0;
			for (long i=0; i < amount; i++){
				if (UniformGen.nextDouble(pilot.getRandomStream(), 0.,1.) < succesProba)
					amountWithSuccess++;
			}
		}
		return amountWithSuccess;
	}

	static public long binomialForSuperIndividual(Pilot pilot, long amount, double succesProba) {
		return binomialForSuperIndividual(pilot, amount, succesProba, 50);
	}

	static public double logNormalAlea(Pilot pilot, double mean, double cv){
		if (cv > 0.){
			double mu=Math.log(mean)- (cv *cv)/2.;
			double sigma = Math.sqrt(Math.log(cv*cv +1));
			return Math.exp((UniformGen.nextDouble(pilot.getRandomStream(),0.,1. ) * sigma + mu));
		}
		else
			return mean;
	}


}
