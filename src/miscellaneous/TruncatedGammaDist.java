package miscellaneous;

import umontreal.iro.lecuyer.probdist.GammaDist;

public class TruncatedGammaDist {

	private final double probTroncature;
	private GammaDist gammaDist;
	
	public TruncatedGammaDist(double alpha, double lambda, double probTroncature) {
		gammaDist = new GammaDist(alpha, lambda);
		this.probTroncature = probTroncature;
	}

	public double cdf(double x) {
		return Math.max(0, (gammaDist.cdf(x)- probTroncature) / (1-probTroncature));
	}
	
	public double getXTrucature(){
		return gammaDist.inverseF(probTroncature);
	}

}
