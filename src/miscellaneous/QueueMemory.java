package miscellaneous;

import java.util.concurrent.ArrayBlockingQueue;

public class QueueMemory<E> extends ArrayBlockingQueue<E> {

	private int memorySize; 

	public QueueMemory(int memorySize) {
		super(memorySize);
		this.memorySize = memorySize;
	}

	public void push(E item){
		try {
			if (this.size() == this.memorySize){
				this.poll();
			}
			this.put(item);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public E getLastItem(){
		E[] items= (E[]) this.toArray();
		if (items.length > 0)
			return items[items.length-1];
		else
			return null;
	}

	public double getMean(){ 
		double sum= 0;
		for (E item : this){
			sum +=  this.doubleValue(item);
		}
		sum = sum / memorySize ;
		return (sum);
	}

	public double getGeometricMean(){
		double sum= 0;
		for (E item : this){
			sum += Math.log(this.doubleValue(item));
		}
		return (Math.exp(sum / memorySize));
	}

	public double getStandartDeviation(){ 
		double mean= this.getMean();
		double sse= 0;
		for (E item : this){
			sse +=  Math.pow(this.doubleValue(item) - mean, 2.);
		}
		return (Math.sqrt(sse /(memorySize -1.)));		
	}

	public double getCoefficientVariation(){
		return (this.getStandartDeviation() / this.getMean());
	}

	private double doubleValue(E item){
		if (item instanceof Double)
			return ((Double) item);
		else if (item instanceof Float)
			return ((Float) item).doubleValue();
		else if (item instanceof Long)
			return ((Long) item).doubleValue();
		else if (item instanceof Integer)
			return ((Integer) item).doubleValue();
		else
			return Double.NaN;
	}
}
